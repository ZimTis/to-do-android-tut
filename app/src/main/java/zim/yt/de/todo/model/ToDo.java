package zim.yt.de.todo.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author ZimTis on 29.06.2016.
 */
public class ToDo implements Serializable {
    private long     id;
    private String   name;
    private Calendar dueDate;
    private boolean  important;
    private String   description;
    private LatLng   location;

    public ToDo() {
        this(null, null, false, null, null);
    }

    public ToDo(final String name) {
        this(name, null, false, null, null);
    }

    public ToDo(final String name, final Calendar dueDate, final boolean important, final String description, final LatLng location) {
        this.name = name;
        this.dueDate = dueDate;
        this.important = important;
        this.description = description;
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Calendar getDueDate() {
        return dueDate;
    }

    public void setDueDate(final Calendar dueDate) {
        this.dueDate = dueDate;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public boolean isImportant() {
        return important;
    }

    public void setImportant(final boolean important) {
        this.important = important;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(final LatLng location) {
        this.location = location;
    }
}
