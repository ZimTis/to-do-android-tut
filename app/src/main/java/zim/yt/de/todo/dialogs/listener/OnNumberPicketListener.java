package zim.yt.de.todo.dialogs.listener;

import android.support.v4.app.DialogFragment;

/**
 * @author ZimTis on 11.10.2016.
 */

public interface OnNumberPicketListener {

    void onNumberPicket(final boolean numberPicked, final int number, final DialogFragment dialogFragment);

}
