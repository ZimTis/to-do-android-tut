package zim.yt.de.todo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import zim.yt.de.todo.adapter.listview.ToDoOverviewListAdapter;
import zim.yt.de.todo.database.TodoDatabase;
import zim.yt.de.todo.dialogs.NumberPickerDialogFragment;
import zim.yt.de.todo.dialogs.listener.OnNumberPicketListener;
import zim.yt.de.todo.model.ToDo;

public class TodoOverviewActivity extends AppCompatActivity implements OnNumberPicketListener {

    private ListView                listView;
    private List<ToDo>              dataSource;
    private ToDoOverviewListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_overview);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.listView = (ListView) findViewById(R.id.todos);

        this.dataSource = TodoDatabase.getInstance(this).readAllToDos();

        this.adapter = new ToDoOverviewListAdapter(this, dataSource);
        this.listView.setAdapter(adapter);
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l) {
                Object element = adapterView.getAdapter().getItem(i);

                if (element instanceof ToDo) {
                    ToDo todo = (ToDo) element;

                    Intent intent = new Intent(TodoOverviewActivity.this, ToDoDetailActivity.class);
                    intent.putExtra(ToDoDetailActivity.TODO_ID_KEY, todo.getId());

                    startActivity(intent);
                }

                Log.e("ClickOnList", element.toString());

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshListView();
    }

    private void refreshListView() {
        dataSource.clear();
        dataSource.addAll(TodoDatabase.getInstance(this).readAllToDos());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onNumberPicket(final boolean numberPicked, final int number, final DialogFragment dialogFragment) {
        if (numberPicked) {
            Toast.makeText(this, "neue nummer: " + number, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "abbruch", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_overview_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_clear_all:
                this.clearAll();
                return true;
            case R.id.menu_new_todo:
                this.newTodo();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void clearAll() {
        TodoDatabase database = TodoDatabase.getInstance(TodoOverviewActivity.this);
        database.deleteAllToDos();
        refreshListView();
    }

    public void newTodo(){
        Intent i = new Intent(TodoOverviewActivity.this, CreateNewToDoActivity.class);
        startActivity(i);
    }
}
