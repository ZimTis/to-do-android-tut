package zim.yt.de.todo.adapter.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import zim.yt.de.todo.R;
import zim.yt.de.todo.model.ToDo;

/**
 * @author ZimTis on 29.06.2016.
 */
public class ToDoOverviewListAdapter extends ArrayAdapter<ToDo> {
    public ToDoOverviewListAdapter(final Context context, final List<ToDo> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
        ToDo currentToDo = getItem(position);

        View view = convertView;

        if (view == null) {
            view = LayoutInflater.from(getContext()).inflate(R.layout.todo_overview_listitem, parent, false);
        }

        ((TextView) view.findViewById(R.id.name)).setText(currentToDo.getName());

        TextView dueDate = (TextView) view.findViewById(R.id.dueDateText);
        ImageView important = (ImageView) view.findViewById(R.id.important_icon);

        if (currentToDo.getDueDate() == null) {
            dueDate.setVisibility(View.GONE);
        } else {
            dueDate.setVisibility(View.VISIBLE);
            dueDate.setText(String.valueOf(currentToDo.getDueDate().get(Calendar.YEAR)));
        }

        if(currentToDo.isImportant()){
            important.setVisibility(View.VISIBLE);
            important.setImageResource(R.mipmap.ic_ausrufezeichen);
        }else{
            important.setVisibility(View.INVISIBLE);
        }

        return view;

    }
}
